FROM debian:buster-slim

# Args
ARG backup_user=dceuser
ARG backup_pass=dcebackup123

# Add packages
RUN apt-get update -qy
RUN apt-get upgrade -qy
RUN apt-get install -qy nodejs npm samba

# Create SMB backup folder
RUN mkdir dcebackup/

# Copy smb conf over
COPY smb.conf /etc/samba/smb.conf

RUN testparm

# Restard Samba daemon to ensure conf takes hold
RUN service smbd restart

# Create USER
RUN groupadd dcebackup && useradd -g dcebackup -u 1000 ${backup_user} -p ${backup_pass}

# Add SMB user
RUN (echo ${backup_pass}; echo ${backup_pass}) | smbpasswd -s -a dceuser

#EXPOSE 139/tcp 445/tcp
